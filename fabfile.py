import os
from contextlib import contextmanager
from fabric.api import cd, env, local, prompt, put, prefix, run, shell_env


VENV_PATH = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'venv')
SETTINGS_MODULE = 'agsound.settings'
PYTHON_EXEC = local('which python3.4', capture=True)


def install():
    # create virtualenv
    local('virtualenv -p {} {}'.format(PYTHON_EXEC, VENV_PATH))

    with _venv_local():
        # install requirements
        local('pip install -r requirements.txt')

        # set the database and populate it
        _django_local('makemigrations')
        _django_local('migrate')
        _django_local('loaddata base.json')


def runserver():
    with _venv_local():
        _django_local('runserver')


def _django_local(command):
    return local('python manage.py {}'.format(command))


def test():
    with _venv_local():
        _django_local('test fts --failfast --noinput -v 2')


@contextmanager
def _venv_local():
    with shell_env(DJANGO_SETTINGS_MODULE=SETTINGS_MODULE):
        with prefix('. %s/bin/activate' % VENV_PATH):
            yield
