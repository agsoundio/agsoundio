from django.contrib import admin

from main.models import Sound


class SoundAdmin(admin.ModelAdmin):
    pass

admin.site.register(Sound, SoundAdmin)
