# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Sound',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('sound', models.FileField(max_length=250, upload_to='sound')),
                ('title', models.CharField(validators=[django.core.validators.MinLengthValidator(3)], default='My sound', max_length=30)),
                ('slug', models.SlugField(db_index=False, max_length=30, editable=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
