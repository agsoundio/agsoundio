from django.conf.urls import patterns, url

from main import views


urlpatterns = patterns('',
    url(r'^$', views.SoundListView.as_view(), name='sound_list'),
    url(r'^(?P<pk>\d+)/(?P<slug>[\w_-]*)$', views.SoundDetailView.as_view(), name='sound_detail'),
    url(r'^create', views.SoundCreateView.as_view(), name='sound_create'),
    url(r'^youtube', views.SoundYouTubeView.as_view(), name='sound_yt'),
    url(r'^api', views.API.as_view(), name='api'),
)
