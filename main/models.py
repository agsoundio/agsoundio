from django.db import models
from django.core.validators import MinLengthValidator
from django.utils.text import slugify
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver


class Sound(models.Model):
    sound = models.FileField(max_length=250, upload_to='sound')
    title = models.CharField(max_length=30, validators=[MinLengthValidator(3)], default='My sound')
    slug = models.SlugField(max_length=30, db_index=False, editable=False)
    created = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(self.title).__str__()
        super(Sound, self).save(*args, **kwargs)


@receiver(pre_delete, sender=Sound)
def sound_delete(sender, instance, **kwargs):
    instance.sound.delete(False)
