from django.views.generic import CreateView, DetailView, ListView, FormView, TemplateView

from main.models import Sound
from main.forms import SoundForm, YouTubeForm


class SoundCreateView(CreateView):
    model = Sound
    form_class = SoundForm


class SoundDetailView(DetailView):
    model = Sound


class SoundListView(ListView):
    model = Sound


class SoundYouTubeView(FormView):
    form_class = YouTubeForm
    template_name = 'main/youtube_form.html'
    success_url = '/1/'


class API(TemplateView):
    template_name = 'main/api.json'
