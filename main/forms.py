from django.forms import URLField, Form, ModelForm, IntegerField, CharField

from main.models import Sound


class SoundForm(ModelForm):
    class Meta:
        model = Sound
        fields = ['sound', 'title']

    def clean_title(self):
        return self.cleaned_data['title'].strip()


class YouTubeForm(Form):
    url = URLField(initial='https://www.youtube.com/watch?v=dQw4w9WgXcQ')
    start = CharField(initial='00:00')
    title = CharField(initial='The Good The Bad')
    length = IntegerField(initial=20)
