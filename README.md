# AGSound

## Wizja

AGSound jest rewolucyjnym podejściem do dzielenia się emocjami w Internecie. Dostarcza prosty ale potężny interfejs do tworzenia dźwięków z dowolnych źródeł - filmów z YouTube, muzyki z SoundCloud lub zwykłych plików audio.

Od strony użytkownika wystarczy jeden klik aby cieszyć się wygenerowanym dźwiękiem i dzielić się nim ze znajomymi. Z drugiej strony, dzięki rozbudowanemu API dla programistów, dźwięki mogą być automatycznie tworzone lub osadzane na zewnętrznych stronach internetowych jako dostosowane widgety, a integracja z najpopularniejszymi serwisami społecznościowymi pozwala naszym użytkownikom na dostęp do nich gdziekolwiek są i w każdej formie, niezależnie od urządzenia z jakiego korzystają.

## Changelog

Czyli co zostało już wykonane.

### 0.3

- UC#10 – Pobranie pliku JSON zawierającego informacje o wygenerowanym pliku dźwiękowym

- UC#2 - Wyodrębnienie ścieżki dźwiękowej z pliku w serwisie YouTube
    - Jako użytkownik serwisu po znalezieniu ciekawego pliku video w serwisie YouTube chcę wyodrębnić część jego ścieżki dźwiękowej.

### 0.2

- UC#3 – Uzyskanie linku do stworzonego pliku dźwiękowego
    - Jako użytkownik serwisu chcę móc uzyskać link do stworzonego pliku dźwiękowego dostępnego online

- UC#4 – Pobranie pliku dźwiękowego w formacie MP3
    - Jako użytkownik serwisu chcę mieć możliwość ściągnięcia dostępnego w serwisie dźwięku na swój dysk w formacie MP3

### 0.1

- UC#1 – Stworzenie dźwięku w serwisie z pliku audio na dysku użytkownika

## Roadmap

Czyli plan na przyszłe iteracje.

### 0.4

- UC#9 – Osadzenie jednego z dźwięków na zewnętrznej stronie internetowej
    - Jako właściciel strony WWW chcę szybko osadzić jeden z dostępnych w Serwisie dźwięków na mojej stronie internetowej.

- UC#5 - Oznaczanie dźwięków jako ulubione

### 0.5

- UC#8 – Tworzenie playlisty z dostępnych dźwięków

### 0.6

- UC#6 – Dodanie komentarza do pliku dźwiękowego

### 0.7

- UC#7 – Ocenianie plików dźwiękowych dostępnych w serwisie

## Technologie

Do wykonania projektu zostanie wykorzystany język Python w wersji 3.4 oraz framework Django w wersji 1.8.5. Jako narzędzia pomocnicze będą służyć nam ostatnie wersje bibliotek JQuery, Bootstrap oraz programu aconv.

## Przydatne linki i pomoc

- Repozytorium https://bitbucket.org/agsoundio/agsoundio

- Wiki https://bitbucket.org/agsoundio/agsoundio/wiki/Home

- Issue tracker https://bitbucket.org/agsoundio/agsoundio/issues

## Dokumentacja

Dokumentację projektu można znaleść w katalogu `docs` - https://bitbucket.org/agsoundio/agsoundio/src

## Instalacja

W celu uruchomienia projektu należy:

- Sklonować to repozytorium

- `pip install fabric virtualenv`

- `fab install` (w katalogu głównym projektu)

- `fab runserver`

Projekt będzie dostępny pod adresem `http://localhost:8000`.

Panel administracyjny można zobaczyć na `http://localhost:8000/admin`. Dane do logowania to admin:admin.

## Plan wdrożenia

Projekt już od pierwszej iteracji jest w pełni gotowy do wdrożenia. Docelowym serwerem produkcyjnym jest konto w serwisie PythonAnywhere. Aby umieścić tam aktualną wersję projektu należy:

- Założyć konto na PythonAnywhere (lub zalogować się na istniejące)

- Utworzyć nową Aplikację

- W folderze aplikacji wykonać polecenie sklonowania tego repozytorium

- Dokonać restartu Aplikacji

- W przyszłości proces ten zostanie całkowicie zautomatyzowany.

## Kontakt

Autorami projektu są Kamil Bojeś oraz Mateusz Gawron.

W razie jakichkolwiek pytań lub wątpliwości prosimy o kontakt z naszym zespołem:

- poprzez e-mail agsoundio@gmail.com

- poprzez BitBucket https://bitbucket.org/agsoundio/agsoundio