import os
from selenium import webdriver
from time import sleep

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test.utils import override_settings


from main.models import Sound


CHROMEDRIVER_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/chromedriver/chromedriver'
EXAMPLE_AUDIO_FILE = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/media/sound/atoms.mp3'


@override_settings(DEBUG=True)
class SoundTestCase(StaticLiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Chrome(CHROMEDRIVER_PATH)

    def tearDown(self):
        self.browser.quit()

    def get(self, url):
        self.browser.get(self.live_server_url + url)

    def test_sound_create(self):
        # Florence hits the homepage.
        self.get('/create')
        sleep(2)

        # She fills the form with her audio file and title of her desire.
        file_input = self.browser.find_element_by_id('id_sound')
        file_input.clear()
        file_input.send_keys(EXAMPLE_AUDIO_FILE)
        sleep(2)

        title_input = self.browser.find_element_by_id('id_title')
        title_input.clear()
        title_input.send_keys('Example title')
        sleep(2)

        # She clicks submit button.
        form = self.browser.find_element_by_tag_name('form')
        form.submit()
        sleep(2)

        # She is now on a sound detail page.
        last_id = Sound.objects.latest('id').id
        detail_url = self.live_server_url + '/' + str(last_id) + '/example-title'
        current_url = self.browser.current_url
        self.assertEqual(current_url, detail_url)

        # She sees a player with her sound.
        self.browser.find_element_by_tag_name('audio')
        sleep(5)
